const sass = require('sass'),
	fs = require("fs"),
	path = require("path");

let devFunc = () => {
    let inPath = "./sass/";
    let outPath = "./public/css/";
    let files = fs.readdirSync(inPath);

    files.forEach((file) => {
	    let result = sass.compile(path.join(inPath, file));
	    fs.writeFile(path.join(outPath, path.parse(file).name + ".css"), result.css, err => {});
    })
}

module.exports = devFunc;