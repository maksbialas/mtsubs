const express = require('express'),
	http = require('http'),
	path = require('path'),
	devScript = require("./dev-script"),
	app = express();

const PORT = 3000;

if (process.argv.includes("__DEV__")) {
    devScript();
}

const router = express.Router();

router.get("/", (req, res) => {
    res.sendFile(path.join(__dirname + "/index.html"));
});

app.use(express.static(__dirname + "/public"));
app.use("/", router);

app.get('*', function(req, res) {
    res.redirect('/');
});

const server = http.createServer(app);

server.listen(PORT, () => {
    console.log(`listening on localhost:${PORT}`);
})