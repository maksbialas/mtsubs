minus.onclick = () => vtt.removeOffset('video', 0.5);
plus.onclick = () => vtt.addOffset('video', 0.5);

track.oncuechange = event => {
    let activeCues = video.textTracks[0].activeCues;
    translation.innerText = "";

    if (activeCues.length) {
        original.innerText = activeCues[0].text;
    } else {
        original.innerText = "";
    }
}

original.addEventListener('mouseup', e => {
    let selection = window.getSelection().toString();
    if (selection.trim()) {
        translateAndSet(selection.trim(), "JA", "EN");
    }
});

const translateAndSet = (text, sourceLang, targetLang) => {

    fetch("https://api-free.deepl.com/v2/translate", {
        body: `auth_key=c941a8f9-cc90-a1e6-efe5-293038203eed&text=${text}&target_lang=${targetLang}&source_lang=${sourceLang}`,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        method: "POST"
    }).then(
        response => response.json()
    ).then(
        json => {
            translation.innerText = json["translations"][0]["text"];
        }
    );

};


// Play button action

playpause.addEventListener('click', function(e) {
    if (video.paused || video.ended) video.play();
    else video.pause();
});

video.addEventListener('play', function() {
    playpause.setAttribute('data-state', 'pause');
}, false);
video.addEventListener('pause', function() {
    playpause.setAttribute('data-state', 'play');
}, false);


// 10sec buttons action
minusten.addEventListener('click', function(e) {
    video.currentTime -= 10;
});

plusten.addEventListener('click', function(e) {
    video.currentTime += 10;
});


// Progress action
video.addEventListener('timeupdate', function() {
	let date = new Date(0);
	date.setSeconds(video.currentTime);
	date = date.toISOString();
	if (date[12] == "0") {
	    curtime.innerText = date.substr(14, 5);
	} else {
		curtime.innerText = date.substr(12, 7);
	}
});

// Fullscreen action

var isFullScreen = function() {
    return !!(document.fullscreen || document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement || document.fullscreenElement);
}

var handleFullscreen = function() {
    if (isFullScreen()) {
        fs.setAttribute('data-state', 'go');
        if (document.exitFullscreen) document.exitFullscreen();
        else if (document.mozCancelFullScreen) document.mozCancelFullScreen();
        else if (document.webkitCancelFullScreen) document.webkitCancelFullScreen();
        else if (document.msExitFullscreen) document.msExitFullscreen();
    } else {
        fs.setAttribute('data-state', 'exit');
        if (videoContainer.requestFullscreen) videoContainer.requestFullscreen();
        else if (videoContainer.mozRequestFullScreen) videoContainer.mozRequestFullScreen();
        else if (videoContainer.webkitRequestFullScreen) videoContainer.webkitRequestFullScreen();
        else if (videoContainer.msRequestFullscreen) videoContainer.msRequestFullscreen();
    }
}

fs.addEventListener('click', function(e) {
    handleFullscreen();
});